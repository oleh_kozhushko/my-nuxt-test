---
big:
  names:
    - your image: /img/img4.jpg
      small desc: my small desdc
      large desc: lorem ipsum which you can regard as yours
    - your image: /img/img2.jpg
      small desc: fefrfb 4g54g4g
      large desc: loremjerbfg3bg34ubg guh43ugh54ugh4ghu4g g45ugh45uhg 4ugh45guh45
        4bg45ugb54ubg 4gu45hbg45ubg gj4ngu45bgubg
  description: yeah my description i didn't see
rating_section: "34"
explainer_section:
  title:
    de: erghtrhh
    en: hrthtrh
  description:
    de: rhrthr
    en: htrhtr
  explanations:
    explanation1:
      steps:
        - image: /img/img1.webp
          title:
            de: hnttjyjtjtj
            en: tyjtyjj
          description:
            de: jytjtjtj
            en: tjtjtjtjjytjjtj
    explanation2:
      steps:
        - image: /img/screenshot-from-2020-12-14-17-22-27.png
          title:
            de: tyjtjt
            en: tjtyj
          description:
            de: domised words which don't look even slightly believable. If you are going to
              use a passage of Lorem Ipsum, you need to be sure there isn't
              anything embarrassing hidden in the middle of text. All the Lorem
              Ips
            en: domised words which don't look even slightly believable. If you are going to
              use a passage of Lorem Ipsum, you need to be sure there isn't
              anything embarrassing hidden in the middle of text. All the Lorem
              Ips
    explanation3:
      title:
        de: domised words which don't look even slightly believable. If you are going to
          use a passage of Lorem Ipsum, you need to be sure there isn't anything
          embarrassing hidden in the middle of text. All the Lorem Ips
        en: domised words which don't look even slightly believable. If you are going to
          use a passage of Lorem Ipsum, you need to be sure there isn't anything
          embarrassing hidden in the middle of text. All the Lorem Ips
      description:
        de: domised words which don't look even slightly believable. If you are going to
          use a passage of Lorem Ipsum, you need to be sure there isn't anything
          embarrassing hidden in the middle of text. All the Lorem Ips
        en: domised words which don't look even slightly believable. If you are going to
          use a passage of Lorem Ipsum, you need to be sure there isn't anything
          embarrassing hidden in the middle of text. All the Lorem Ips
      social_images:
        instagram: /img/screenshot-from-2020-12-14-19-17-01.png
        facebook: /img/img4.jpg
        linkedin: /img/img2.jpg
        twitter: /img/img3.webp
        reddit: /img/screenshot-from-2020-12-14-17-22-27.png
        pinterest: /img/img1.webp
name: egrgegegegergegegegeggreg
header:
  sliders:
    - title: My Slide is the best
      smallDesc: My small description here
      images:
        - img/img1.webp
        - img/img2.jpg
    - title: What you clorem lorem lorem
      smallDesc: What you fjeffrh jfh3rfgfyg jf4ufhfhf fkj4rfu4hfuf
      images:
        - img/img3.webp
        - img/img4.jpg
  url: /
  slider:
    - image: /img/screenshot-from-2020-12-14-19-17-01.png
      title:
        de: Weiß nicht, kann auch weg?
        en: ferfrerrgegerg
      additional_title:
        de: Weiß nicht
        en: thrthhh6h565
      link_title:
        de: Weiß nic
        en: hthrthrhrrhrhrhh
      link_url: /56u5u5
      description:
        de: Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie.
          Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text seit
          1500, als ein unbekannter Schriftsteller eine Hand voll Wörter nahm
          und diese durcheinander warf um ein Musterbuch zu erstellen. Es hat
          nicht nur 5 Jahrhunderte überlebt, sondern auch in Spruch in die
          elektronische Schriftbearbeitung geschafft (bemerke, nahezu
          unverändert). Bekannt wurde es 1960, mit dem erscheinen von
          "Letraset", welches Passagen von Lorem Ipsum enhielt, so wie Desktop
          Software wie "Aldus PageMaker" - ebenfalls mit Lorem Ipsum.
        en: Lorem Ipsum is simply dummy text of the printing and typesetting industry.
          Lorem Ipsum has been the industry's standard dummy text ever since the
          1500s, when an unknown printer took a galley of type and scrambled it
          to make a type specimen book. It has survived not only five centuries,
          but also the leap into electronic typesetting, remaining essentially
          unchanged. It was popularised in the 1960s with the release of
          Letraset sheets containing Lorem Ipsum passages, and more recently
          with desktop publishing software like Aldus PageMaker including
          versions of Lorem Ipsum.
      bitcoinAddress: 455446655jytytjjyjy
    - image: /img/img3.webp
      title:
        de: rthrthtrhr rhrthth
        en: rhrhrhttrhtbrh5yhn  56u65u5
      additional_title:
        de: thrhr yjj65
        en: rhrhrhtrhn 56h565
      link_title:
        de: /67i6i676 rh5h
        en: /67i6i6ih455h4h
      link_url: /i67i76i6i754y4y4
      description:
        de: eg45y45
        en: 45y45y45
      bitcoinAddress: 4564646tytytuu
    - image: /img/img4.jpg
      title:
        de: dhrthtrh
        en: rthrhrth
      additional_title:
        de: y5y65y5y
        en: 5y5y5yy65y
      link_title:
        de: 56y5y5
        en: 56y5y56
      link_url: /u7
      description:
        de: 5u56u75u5u
        en: 56u56u65u55
      bitcoinAddress: rhhtrh544646464646
product:
  title:
    de: "Es ist ein lang "
    en: "n the industry's "
  section_description:
    de: Glauben oder nicht glauben
    en: . Many desktop publishing packages and web page editors now u
  product_options:
    option1:
      de: Digitale Pakete
      en: Digital package
    option2:
      de: Physische Pakete
      en: Physical package
  oprion1_products:
    product1:
      title:
        de: Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der
          Industrie bereit
        en: established fact that a reader
      description:
        de: ronische Schriftbearbeitung geschafft (bemerke, nahezu unverändert). Bekannt
          wurde es 1960, mit dem erscheinen von "Letraset", welches Passage
        en: ters, as opposed to using 'Content here, content here', making it look like
          readable English. Many desktop publishing packages and web page
          editors now use Lorem Ipsum as their default model text, a
      price: 32
      features:
        - feature:
            title:
              de: nd diese dur
              en: grergre
            description:
              de: annter Schriftsteller eine Hand voll Wörter nahm und diese durcheinander
              en: " of passages of Lorem Ipsum available, but the majority have suffered
                altera"
        - feature:
            title:
              de: sdfergege
              en: gergegrg
            description:
              de: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
              en: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
    product2:
      title:
        de: u unverändert). Bekannt wurde es 1960, mit dem erscheinen von "Letraset"
        en: " as opposed to using 'Content here, content here', making it look like
          reada"
      description:
        de: kannter Schriftsteller eine Hand voll Wörter nahm und diese durcheinander
          warf um ein Musterbuch zu erstellen. Es hat nicht nur 5 Jahrhunderte
          überlebt, sondern auch in Spruch in die elektronische Schriftbearb
        en: domised words which don't look even slightly believable. If you are going to
          use a passage of Lorem Ipsum, you need to be sure there isn't anything
          embarrassing hidden in the middle of text. All the Lorem Ips
      price: 65
      features:
        - feature:
            title:
              en: "domised words which don't look even slightly believable. "
              de: rthhrtr
            description:
              de: grtggtrgrg
              en: rhrhth
        - feature:
            title:
              de: gtrgrh
              en: hrth
            description:
              de: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
              en: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
  oprion2_products:
    product1:
      title:
        de: htyhtyhtj
        en: tyjtjtj
      description:
        de: tyjtj
        en: tyjtjtj
      features:
        - feature:
            title:
              de: tjytj
              en: tyjtjtjjytj
            description:
              de: tjtjtyjtjtj
              en: tyjytjjtjt
        - feature:
            title:
              de: gergre
              en: ergegeg
            description:
              de: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
              en: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
      price: 45
    product2:
      title:
        de: tjtjyjtjj
        en: tyjytjtyjtyjyt
      description:
        de: tjtyjjtjtj
        en: tjytyjtjytjt
      price: 45
      features:
        - feature:
            title:
              de: httyhty
              en: ythythtyh
            description:
              de: tyhthth
              en: tyhththyyh
        - feature:
            title:
              de: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
              en: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
            description:
              de: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
              en: domised words which don't look even slightly believable. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn't
                anything embarrassing hidden in the middle of text. All the
                Lorem Ips
  gateway_url: /tyytyj
planet_section:
  first_article:
    title:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    additional_title:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    description:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    Button_title:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    button_url: domised words which don't look even slightly believable. If you are
      going to use a passage of Lorem Ipsum, you need to be sure there isn't
      anything embarrassing hidden in the middle of text. All the Lorem Ips
  second_article:
    title:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    additional_title:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    description:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    Button_title:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    button_url: /fergr
  moon_content:
    description:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
  satellite_content:
    description:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
  earch_content:
    description:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
social_urls:
  pinterest: /
  reddit: /
  twitter: /
  linkedin: /
  facebook: /
  instagram: /
title: index
stories_section:
  title:
    de: jytyjyt
    en: tyjtyjjtyjtyjtjtj
  categories:
    - title: tyjtyjtj
      stories:
        - person_name: tjtjtjytj
          title:
            de: tyjty
            en: tyjtj
          description:
            de: tjtj
            en: tytjtyjtyjt
          bitcoin_address: ytjtyjytjt
          button_url: /thrhrthrh
  additional_info:
    content: domised words which don't look even slightly believable. If you are
      going to use a passage of Lorem Ipsum, you need to be sure there isn't
      anything embarrassing hidden in the middle of text. All the Lorem Ips
    button_title:
      de: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
      en: domised words which don't look even slightly believable. If you are going to
        use a passage of Lorem Ipsum, you need to be sure there isn't anything
        embarrassing hidden in the middle of text. All the Lorem Ips
    story_url: /fergre
image: /img/uploads/img3.webp
description: my description is the best
newsletter_section:
  title:
    de: domised words which don't look even slightly believable. If you are going to
      use a passage of Lorem Ipsum, you need to be sure there isn't anything
      embarrassing hidden in the middle of text. All the Lorem Ips
    en: domised words which don't look even slightly believable. If you are going to
      use a passage of Lorem Ipsum, you need to be sure there isn't anything
      embarrassing hidden in the middle of text. All the Lorem Ips
  description:
    de: domised words which don't look even slightly believable. If you are going to
      use a passage of Lorem Ipsum, you need to be sure there isn't anything
      embarrassing hidden in the middle of text. All the Lorem Ips
    en: domised words which don't look even slightly believable. If you are going to
      use a passage of Lorem Ipsum, you need to be sure there isn't anything
      embarrassing hidden in the middle of text. All the Lorem Ips
  bitcoin_address: fewfefefrefeggreg
  button_url: /tyjy
Rating:
  title:
    de: egrggg
    en: regegegeg
  rating_slider:
    - review:
        de: gtg45ggg
        en: tgg5g5g
      rating: 45
      user:
        avatar: /img/img2.jpg
        user_name: t45t54t4t 56h65h56h
        user_info: b54t4ty44t 5g565h
---

- fjr3i3j4i43h3hr
- 3rjfi3nf34f3ufbubf
- rjf3ihf34uf3f3u433uhf
- rijfrifhrufh34ufe3uffub
- 3ofj3ifj34if3fi 3ifj3ifh3if fi3f3i4hf
- fi3rjfi34 3jfdj3i4jf43 j3nfi43fn i4fj3irh34

[About](/about)
